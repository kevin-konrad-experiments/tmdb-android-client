package com.konradkevin.mobiapps.di.modules

import com.konradkevin.mobiapps.di.ViewModelFactory
import androidx.lifecycle.ViewModel
import com.konradkevin.mobiapps.data.repositories.MoviesRepository
import kotlin.reflect.KClass
import com.konradkevin.mobiapps.ui.movies.MoviesViewModel
import com.konradkevin.mobiapps.ui.movies.details.MoviesDetailsViewModel
import com.konradkevin.mobiapps.ui.movies.list.MoviesListViewModel
import dagger.*
import dagger.multibindings.IntoMap
import dagger.Provides
import javax.inject.Provider


@Module
object ViewModelsModule {

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Reusable
    @JvmStatic
    @Provides
    fun providesViewModelFactory(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelFactory = ViewModelFactory(providerMap)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    fun providesMoviesViewModel(): ViewModel = MoviesViewModel()

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(MoviesDetailsViewModel::class)
    fun providesMoviesDetailsViewModel(moviesRepository: MoviesRepository): ViewModel = MoviesDetailsViewModel(moviesRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(MoviesListViewModel::class)
    fun providesMoviesListViewModel(moviesRepository: MoviesRepository): ViewModel = MoviesListViewModel(moviesRepository)
}