package com.konradkevin.mobiapps.di.modules

import android.content.Context
import androidx.room.Room
import com.konradkevin.mobiapps.BuildConfig
import com.konradkevin.mobiapps.data.local.database.AppDatabase
import com.konradkevin.mobiapps.data.local.database.dao.MovieDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME)
        // TODO : don't allow database accesses on the main thread
        .allowMainThreadQueries()
        .build()

    @Reusable
    @JvmStatic
    @Provides
    fun provideMovieDao(appDatabase: AppDatabase): MovieDao = appDatabase.movieDao()
}