package com.konradkevin.mobiapps.di.components

interface DaggerComponentProvider {
    val component: ApplicationComponent
}