package com.konradkevin.mobiapps.di.components

import android.content.Context
import com.konradkevin.mobiapps.di.modules.DatabaseModule
import com.konradkevin.mobiapps.di.modules.NetworkModule
import com.konradkevin.mobiapps.di.modules.ViewModelsModule
import com.konradkevin.mobiapps.ui.movies.details.MoviesDetailsFragment
import com.konradkevin.mobiapps.ui.movies.list.MoviesListFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    DatabaseModule::class,
    NetworkModule::class,
    ViewModelsModule::class
])
interface ApplicationComponent {

    fun inject(fragment: MoviesDetailsFragment)
    fun inject(fragment: MoviesListFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun networkModule(networkModule: NetworkModule): Builder
        fun viewModelsModule(viewModelsModule: ViewModelsModule): Builder
        fun build(): ApplicationComponent
    }
}
