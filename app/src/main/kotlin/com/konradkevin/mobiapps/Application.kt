package com.konradkevin.mobiapps

import android.app.Application
import androidx.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.konradkevin.mobiapps.di.components.ApplicationComponent
import com.konradkevin.mobiapps.di.components.DaggerApplicationComponent
import com.konradkevin.mobiapps.di.components.DaggerComponentProvider
import com.konradkevin.mobiapps.di.modules.DatabaseModule
import com.konradkevin.mobiapps.di.modules.NetworkModule
import com.konradkevin.mobiapps.di.modules.ViewModelsModule
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber

class Application : Application(), DaggerComponentProvider {
    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .databaseModule(DatabaseModule)
            .networkModule(NetworkModule)
            .viewModelsModule(ViewModelsModule)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        initStetho()
        initTimber()
        //initLeakCanary()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initLeakCanary() {
        if (BuildConfig.DEBUG && !LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this)
        }
    }
}