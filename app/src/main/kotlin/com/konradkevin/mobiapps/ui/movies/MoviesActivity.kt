package com.konradkevin.mobiapps.ui.movies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.konradkevin.mobiapps.R

class MoviesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        setSupportActionBar(findViewById(R.id.toolbar))
    }
}
