package com.konradkevin.mobiapps.ui.movies

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class MoviesViewModel @Inject constructor() : ViewModel()