package com.konradkevin.mobiapps.ui.movies.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.konradkevin.mobiapps.data.Data
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity
import com.konradkevin.mobiapps.data.repositories.MoviesRepository
import javax.inject.Inject

class MoviesListViewModel @Inject constructor(private val moviesRepository: MoviesRepository) : ViewModel() {
    private val _page: MutableLiveData<Int> = MutableLiveData(1)
    private val _data: LiveData<Data<List<MovieEntity>>> = Transformations.switchMap(_page) { moviesRepository.getMovies(it) }

    val movies: LiveData<List<MovieEntity>?> = Transformations.map(_data) { it.data }
    val loading: LiveData<Boolean> = Transformations.map(_data) { it.status == Data.Status.LOADING }
    val error: LiveData<String?> = Transformations.map(_data) { it.error?.localizedMessage }

    fun loadNextPage() {
        _page.value = _page.value?.plus(1)
    }
}