package com.konradkevin.mobiapps.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.mobiapps.di.ViewModelFactory
import javax.inject.Inject

abstract class BaseFragment<M: ViewModel, B: ViewDataBinding> constructor(@LayoutRes val layoutRes: Int, private val vmClass: Class<M>): Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    protected lateinit var binding: B
    protected lateinit var viewModel: M

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(vmClass)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        initBinding()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initObservers()
        initTransitions()
    }

    abstract fun inject()
    abstract fun initBinding()
    open fun initObservers() = Unit
    open fun initUi() = Unit
    open fun initTransitions() = Unit
}