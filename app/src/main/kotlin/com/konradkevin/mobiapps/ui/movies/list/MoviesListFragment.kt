package com.konradkevin.mobiapps.ui.movies.list

import android.view.ViewTreeObserver
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.konradkevin.mobiapps.R
import com.konradkevin.mobiapps.databinding.FragmentMoviesListBinding
import com.konradkevin.mobiapps.ui.base.BaseFragment
import com.konradkevin.mobiapps.utils.injector
import kotlinx.android.synthetic.main.fragment_movies_list.*

class MoviesListFragment: BaseFragment<MoviesListViewModel, FragmentMoviesListBinding>(R.layout.fragment_movies_list, MoviesListViewModel::class.java) {

    private val moviesListAdapter = MoviesListAdapter()

    private val onScrollListener = object: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
            if (linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 2) {
                viewModel.loadNextPage()
            }
        }
    }

    override fun inject() {
        injector.inject(this)
    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.adapter = moviesListAdapter
    }

    override fun initUi() {
        movies_list_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = moviesListAdapter
            it.addOnScrollListener(onScrollListener)
            it.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }
    }

    override fun initObservers() {
        viewModel.movies.observe(viewLifecycleOwner, Observer { movies ->
            movies?.let { moviesListAdapter.submitList(it) }
        })
    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }
}