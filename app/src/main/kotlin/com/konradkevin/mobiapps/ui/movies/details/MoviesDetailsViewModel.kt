package com.konradkevin.mobiapps.ui.movies.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.konradkevin.mobiapps.data.Data
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity
import com.konradkevin.mobiapps.data.repositories.MoviesRepository
import javax.inject.Inject

class MoviesDetailsViewModel @Inject constructor(private val moviesRepository: MoviesRepository) : ViewModel() {
    private val _id: MutableLiveData<String> = MutableLiveData()
    private val _data: LiveData<Data<MovieEntity>> = Transformations.switchMap(_id) { moviesRepository.getMovie(it) }

    val title: LiveData<String> = Transformations.map(_data) { it.data?.title }
    val originalTitle: LiveData<String> = Transformations.map(_data) { it.data?.originalTitle }
    val overview: LiveData<String> = Transformations.map(_data) { it.data?.overview }
    val posterPath: LiveData<String> = Transformations.map(_data) { it.data?.posterPath }
    val loading: LiveData<Boolean> = Transformations.map(_data) { it.status == Data.Status.LOADING }
    val error: LiveData<String?> = Transformations.map(_data) { it.error?.localizedMessage }

    fun setMovieId(id: String) {
        _id.value = id
    }
}