package com.konradkevin.mobiapps.ui.movies.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.konradkevin.mobiapps.BuildConfig
import com.konradkevin.mobiapps.R
import com.konradkevin.mobiapps.databinding.FragmentMoviesDetailsBinding
import com.konradkevin.mobiapps.ui.base.BaseFragment
import com.konradkevin.mobiapps.utils.injector
import kotlinx.android.synthetic.main.fragment_movies_details.*

class MoviesDetailsFragment : BaseFragment<MoviesDetailsViewModel, FragmentMoviesDetailsBinding>(R.layout.fragment_movies_details, MoviesDetailsViewModel::class.java) {

    private val args: MoviesDetailsFragmentArgs by navArgs()

    private val requestListener = object: RequestListener<Drawable> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
            startPostponedEnterTransition()
            return false
        }

        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            startPostponedEnterTransition()
            return false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setMovieId(args.movieId)
    }

    override fun inject() {
        injector.inject(this)
    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.moviesDetailsPoster.transitionName = args.movieId + "poster"
    }

    override fun initObservers() {
        viewModel.posterPath.observe(viewLifecycleOwner, Observer {
            it?.let { posterPath -> loadImage(posterPath) }
        })
    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }

    private fun loadImage(imageUrl: String) {
        Glide.with(this)
            .load(BuildConfig.IMAGES_BASE_URL + imageUrl)
            .listener(requestListener)
            .into(movies_details_poster)
    }
}