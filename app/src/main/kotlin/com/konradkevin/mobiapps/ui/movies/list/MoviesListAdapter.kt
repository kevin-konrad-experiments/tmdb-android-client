package com.konradkevin.mobiapps.ui.movies.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.konradkevin.mobiapps.R
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity
import com.konradkevin.mobiapps.databinding.FragmentMoviesListItemBinding
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_movies_list_item.view.*

class MoviesListAdapter : ListAdapter<MovieEntity, MoviesListAdapter.ViewHolder>(MoviesListAdapter.DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentMoviesListItemBinding>(LayoutInflater.from(parent.context), R.layout.fragment_movies_list_item, parent, false)
        return ViewHolder(binding.root, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { movie ->
            holder.itemView.tag = movie
            holder.itemView.movies_item_poster.transitionName = movie.id + "poster"
            holder.itemView.setOnClickListener {
                holder.itemView.findNavController().navigate(
                    R.id.action_moviesListFragment_to_moviesDetailsFragment,
                    bundleOf("movieId" to movie.id),
                    null,
                    FragmentNavigatorExtras(holder.itemView.movies_item_poster to movie.id + "poster")
                )
            }
            holder.bind(movie)
        }
    }

    class ViewHolder(override val containerView: View, private val binding: FragmentMoviesListItemBinding) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(movie: MovieEntity) {
            binding.movie = movie
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<MovieEntity>() {
        override fun areItemsTheSame(oldItem: MovieEntity, newItem: MovieEntity) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: MovieEntity, newItem: MovieEntity) = oldItem == newItem
    }
}
