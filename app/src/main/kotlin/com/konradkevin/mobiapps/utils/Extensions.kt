package com.konradkevin.mobiapps.utils

import android.app.Activity
import androidx.fragment.app.Fragment
import com.konradkevin.mobiapps.di.components.DaggerComponentProvider


val Activity.injector get() = (application as DaggerComponentProvider).component
val Fragment.injector get() = (activity?.application as DaggerComponentProvider).component
