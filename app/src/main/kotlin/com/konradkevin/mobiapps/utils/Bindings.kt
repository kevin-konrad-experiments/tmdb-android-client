package com.konradkevin.mobiapps.utils

import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.bumptech.glide.Glide
import com.konradkevin.mobiapps.BuildConfig

@BindingConversion
fun convertBooleanToVisibility(isVisible: Boolean): Int {
    return if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun bindImageFromSrcUrl(view: ImageView, url: String?) {
    url?.also {
        Glide.with(view.context)
            .load(BuildConfig.IMAGES_BASE_URL + url)
            .preload()

        Glide.with(view.context)
            .load(BuildConfig.IMAGES_BASE_URL + url)
            .into(view)
    }
}

@BindingAdapter("error")
fun bindLayoutError(view: ConstraintLayout, error: String?) {
    error?.let {
        Toast.makeText(view.context, it, Toast.LENGTH_LONG).show()
    }
}