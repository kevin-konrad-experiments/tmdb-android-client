package com.konradkevin.mobiapps.data.remote.tmdb.dto

import com.konradkevin.mobiapps.data.local.database.entities.GenreEntity
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDto(
    @Json(name = "id")
    val id: String,

    @Json(name = "original_language")
    val originalLanguage: String,

    @Json(name = "original_title")
    val originalTitle: String,

    @Json(name = "overview")
    val overview: String,

    @Json(name = "popularity")
    val popularity: Float,

    @Json(name = "release_date")
    val releaseDate: String,

    @Json(name = "adult")
    val adult: Boolean,

    @Json(name = "poster_path")
    val posterPath: String?,

    @Json(name = "backdrop_path")
    val backdropPath: String?,

    @Json(name = "title")
    val title: String,

    @Json(name = "vote_average")
    val voteAverage: Float,

    @Json(name = "vote_count")
    val voteCount: Int,

    @Json(name = "imdb_id")
    val imdbId: String?,

    @Json(name = "revenue")
    val revenue: Long?,

    @Json(name = "runtime")
    val runtime: Int?,

    @Json(name = "homepage")
    val homepage: String?,

    @Json(name = "budget")
    val budget: Long?,

    @Json(name = "status")
    val status: String?,

    @Json(name = "tagline")
    val tagline: String?,

    @Json(name = "genres")
    val genres: List<GenreEntity>?
) {
    fun toEntity(): MovieEntity {
        return MovieEntity(
            id,
            voteCount,
            voteAverage,
            title,
            popularity,
            posterPath,
            originalLanguage,
            originalTitle,
            backdropPath,
            adult,
            overview,
            releaseDate,
            imdbId,
            revenue,
            runtime,
            homepage,
            budget,
            status,
            tagline
        )
    }
}