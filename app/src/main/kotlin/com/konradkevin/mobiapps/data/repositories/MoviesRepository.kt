package com.konradkevin.mobiapps.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.github.leonardoxh.livedatacalladapter.Resource
import com.konradkevin.mobiapps.data.Data
import com.konradkevin.mobiapps.data.remote.tmdb.TmdbService
import com.konradkevin.mobiapps.data.local.database.dao.MovieDao
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity
import com.konradkevin.mobiapps.data.remote.tmdb.dto.DiscoverMoviesDto
import com.konradkevin.mobiapps.data.remote.tmdb.dto.MovieDto
import javax.inject.Inject

class MoviesRepository @Inject constructor(private val TmdbService: TmdbService, private val movieDao: MovieDao) {

    fun getMovies(page: Int): LiveData<Data<List<MovieEntity>>> {
        val mediator: MediatorLiveData<Data<List<MovieEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getMoviesFromDb(page)) {
            mediator.value = Data.success(it)
        }

        mediator.addSource(getMoviesFromApi(page)) {
            when (it.isSuccess) {
                true -> movieDao.insertAll(it.resource?.movies?.map { movieDto ->  movieDto.toEntity() })
                false -> mediator.value = Data.error(it.error)
            }
        }

        return mediator
    }

    fun getMovie(movieId: String): LiveData<Data<MovieEntity>> {
        val mediator: MediatorLiveData<Data<MovieEntity>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getMovieFromDb(movieId)) {
            mediator.value = Data.success(it)
        }

        mediator.addSource(getMovieFromApi(movieId)) {
            when (it.isSuccess) {
                true -> movieDao.insert(it.resource?.toEntity())
                false -> mediator.value = Data.error(it.error)
            }
        }

        return mediator
    }

    private fun getMoviesFromApi(page: Int): LiveData<Resource<DiscoverMoviesDto>> {
        return TmdbService.getPopularMovies(page)
    }

    private fun getMoviesFromDb(page: Int): LiveData<List<MovieEntity>> {
        return movieDao.getAll(page)
    }

    private fun getMovieFromApi(movieId: String): LiveData<Resource<MovieDto>> {
        return TmdbService.getMovie(movieId)
    }

    private fun getMovieFromDb(movieId: String): LiveData<MovieEntity> {
        return movieDao.getOne(movieId)
    }
}