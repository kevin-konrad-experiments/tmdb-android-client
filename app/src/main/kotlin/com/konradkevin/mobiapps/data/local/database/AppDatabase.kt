package com.konradkevin.mobiapps.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.konradkevin.mobiapps.data.local.database.dao.MovieDao
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity

@Database(
    version = 1,
    entities = [
        MovieEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}