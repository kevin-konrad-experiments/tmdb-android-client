package com.konradkevin.mobiapps.data.local.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.konradkevin.mobiapps.data.local.database.entities.MovieEntity

@Dao
interface MovieDao: BaseDao<MovieEntity> {
    @Query("SELECT * from movies ORDER BY popularity DESC LIMIT 0, :page*20")
    fun getAll(page: Int): LiveData<List<MovieEntity>>

    @Query("SELECT * from movies WHERE id = :movieId")
    fun getOne(movieId: String): LiveData<MovieEntity>
}