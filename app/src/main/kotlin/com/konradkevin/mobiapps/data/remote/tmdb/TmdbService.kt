package com.konradkevin.mobiapps.data.remote.tmdb

import androidx.lifecycle.LiveData
import com.github.leonardoxh.livedatacalladapter.Resource
import com.konradkevin.mobiapps.BuildConfig
import com.konradkevin.mobiapps.data.remote.tmdb.dto.DiscoverMoviesDto
import com.konradkevin.mobiapps.data.remote.tmdb.dto.MovieDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TmdbService {
    @GET("discover/movie")
    fun getPopularMovies(
        @Query("page") page: Int,
        @Query("api_key") apiKey: String = BuildConfig.TMDB_API_KEY
    ): LiveData<Resource<DiscoverMoviesDto>>

    @GET("movie/{movieId}")
    fun getMovie(
        @Path("movieId") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDB_API_KEY
    ): LiveData<Resource<MovieDto>>
}